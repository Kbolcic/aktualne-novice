from tkinter import *

import re
import urllib.request
import time
import random
import requests
import webbrowser


slikee=['s1.png','s2.png','s3.png','s4.png','s5.png','s6.png','s7.png','s8.png','s9.png','s10.png','s11.png','s12.png','s13.png','s14.png','s15.png','s16.png']

url1='http://www.24ur.com'
url2='http://www.regionalobala.si/'
url3='http://www.slovenskenovice.si'
url4='http://www.rtvslo.si'
url5='http://www.nogomania.com'



class Novice():

    def __init__(self,master):
        self.platno=Canvas(master,width=1000,height=900,bg='PaleTurquoise1')
        self.platno.grid(row=1,column=1)
        self.napisi()

        
        #GUMBI 
        button1 = Button(master, text = "O programu", command = self.oprogramu, anchor = W)
        button1.configure(width = 50, relief = RAISED)
        button1_window = self.platno.create_window(50, 310, anchor=NW, window=button1) 

        button2 = Button(master, text = "24ur.com", command = self.preberi24, anchor = W)
        button2.configure(width = 30, activebackground = "royal blue", relief = GROOVE)
        button2_window = self.platno.create_window(50, 250, anchor=NW, window=button2) 

        button3 = Button(master, text = "Regional obala", command = self.preberiregional, anchor = W)
        button3.configure(width = 30, activebackground = "orange", relief = GROOVE)
        button3_window = self.platno.create_window(50, 220, anchor=NW, window=button3) 

        button4 = Button(master, text = "Slovenske novice", command = self.preberislovenske, anchor = W)
        button4.configure(width = 30, activebackground = "orange red", relief = GROOVE)
        button4_window = self.platno.create_window(50, 190, anchor=NW, window=button4) 

        button5 = Button(master, text = "Rtvslo MMC", command = self.preberimmc, anchor = W)
        button5.configure(width = 30, activebackground = "khaki", relief = GROOVE)
        button5_window = self.platno.create_window(50, 160, anchor=NW, window=button5) 

        button6 = Button(master, text = "Nogomania", command = self.preberinogomania, anchor = W)
        button6.configure(width = 30, activebackground = "light steel blue", relief = GROOVE)
        button6_window = self.platno.create_window(50, 130, anchor=NW, window=button6) 


        
        button7 = Button(master, text = "Poveži", command = self.povezi1, anchor = W)
        button7.configure(width = 10, activebackground = "light steel blue", relief = GROOVE)
        button7_window = self.platno.create_window(350, 250, anchor=NW, window=button7) 


        button8 = Button(master, text = "Poveži", command = self.povezi2, anchor = W)
        button8.configure(width = 10, activebackground = "light steel blue", relief = GROOVE)
        button8_window = self.platno.create_window(350, 220, anchor=NW, window=button8) 

        
        button9 = Button(master, text = "Poveži", command = self.povezi3, anchor = W)
        button9.configure(width = 10, activebackground = "light steel blue", relief = GROOVE)
        button9_window = self.platno.create_window(350, 190, anchor=NW, window=button9) 

        
        button10 = Button(master, text = "Poveži", command = self.povezi4, anchor = W)
        button10.configure(width = 10, activebackground = "light steel blue", relief = GROOVE)
        button10_window = self.platno.create_window(350, 160, anchor=NW, window=button10) 

        
        button11 = Button(master, text = "Poveži", command = self.povezi5, anchor = W)
        button11.configure(width = 10, activebackground = "light steel blue", relief = GROOVE)
        button11_window = self.platno.create_window(350, 130, anchor=NW, window=button11) 

          #SLIKE
                        
        slikica1=random.choice(slikee)
    def povezi1(self):
        webbrowser.open(url1)
    
    def povezi2(self):
        webbrowser.open(url2)

    def povezi3(self):
        webbrowser.open(url3)

    def povezi4(self):
        webbrowser.open(url4)
    
    def povezi5(self):
        webbrowser.open(url5)




    def napisi(self):
        #BESEDILA
        self.platno.create_text(20,35,text='AKTUALNE NOVICE',anchor=NW,fill='navy',font=('Helvetica',30,'bold'))
        self.platno.create_text(10,880,text='Trenutno prikazujem novice z dne {0}'.format(time.strftime('%d/%m/%y')),anchor=NW,fill='black')

        #ČRTE
        self.platno.create_line(5,115,450,115,fill='navy',width=4) 
        self.platno.create_line(5,350,1000,350,fill='navy',width=6)
        self.platno.create_line(450,5,450,350,fill='navy',width=4)
        self.platno.create_line(5,290,450,290,fill='navy',width=4)
        self.platno.create_line(5,875,1000,875,fill='navy',width=2)
        self.platno.create_line(450,203,1000,203,fill='navy',width=4)

    def preberi24(self):



        
        self.platno.delete('novice')
        
        self.platno.create_text(20,380,text='PRIKAZUJEM NOVICE IZ SPLETNEGA MESTA 24UR',fill='navy',font=('times',15,'bold'),tags='novice',anchor=NW)
        #funkcija novice
        html24=requests.get('http://www.24ur.com')
        novices=html24.text
        novices.strip()
        novice24=re.findall(r'<a class="clearfix" href="(.*)"\s*onclick="pageTracker.',novices,flags=re.DOTALL)
        self.platno.create_text(20,420,text=printaj24(novice24),tags='novice',anchor=NW)
     

        #zgornja slika
        self.slika24=PhotoImage(file='24ur.png')
        self.platno.create_image(452,5,image=self.slika24,anchor='nw',tags='novice')

        #spodje slike
        self.nalozi()
    def preberiregional(self):
        
        self.platno.delete('novice')
        self.platno.create_text(20,380,text='PRIKAZUJEM NOVICE IZ REGIONAL OBALA',fill='navy',font=('times',15,'bold'),tags='novice',anchor=NW)


        #funkcija novice
        html24=requests.get('http://www.regionalobala.si')
        noviceregional=html24.text
        noviceregional.strip()

        noviceregional=re.findall(r'<div class="text"><a href="/novica/(.*)" title="">(.*)</a></div>',noviceregional)
        self.platno.create_text(20,420,text=printajreg(noviceregional),tags='novice',anchor=NW)






        self.slikaregional=PhotoImage(file='1.png')
        self.platno.create_image(452,5,image=self.slikaregional,anchor='nw',tags='novice')
        self.nalozi()
    def preberislovenske(self):

        self.platno.delete('novice')
        self.platno.create_text(20,380,text='PRIKAZUJEM NOVICE IZ SPLETNEGA MESTA SLOVENSKE NOVICE',fill='navy',font=('times',15,'bold'),tags='novice',anchor=NW)

        #funkcija novice
        slovenskehtml=requests.get('http://www.slovenskenovice.si')
        slovenske=slovenskehtml.text
        slovenske.strip()

        novicesl=re.findall(r'<a href="(.*)" title=(.*)</p></a></h1></header>',slovenske)
        self.platno.create_text(20,420,text=printajslovenske(novicesl),tags='novice',anchor=NW)

        #doda sliko
        self.slikasl=PhotoImage(file='sloven.png')
        self.platno.create_image(452,5,image=self.slikasl,anchor='nw',tags='novice')
        self.nalozi()
    def preberinogomania(self):

        self.platno.delete('novice')
        self.platno.create_text(20,380,text='PRIKAZUJEM NOVICE IZ SPLETNEGA MESTA NOGOMANIA',fill='navy',font=('times',15,'bold'),tags='novice',anchor=NW)

        #funkcija novice
        html24=requests.get('http://www.nogomania.com')
        novicenogomania=html24.text
        novicenogomania.strip()

        novicenogomania=re.findall(
                            r'<span style="color:Black; font-size:12px; line-height:15px;">(.*)</span>'
                            ,novicenogomania)
     
        self.platno.create_text(20,420,text=printajnogomania(novicenogomania),tags='novice',font=('times new roman',10,'italic'),anchor=NW)

        self.slikanog=PhotoImage(file='nogoman.png')
        self.platno.create_image(452,5,image=self.slikanog,anchor='nw',tags='novice')
        self.nalozi()

    def preberimmc(self):
        

        self.platno.delete('novice')
        self.platno.create_text(20,380,text='PRIKAZUJEM NOVICE IZ SPLETNEGA MESTA RTV SLO MMC',fill='navy',font=('times',15,'bold'),tags='novice',anchor=NW)

        #funkcija novice
        html24=requests.get('http://www.rtvslo.si')
        novicemmc=html24.text
        novicemmc.strip

        novicemmc=re.findall(r'<span class="text">(.*)</span>',novicemmc)
        self.platno.create_text(20,420,text=printajmmc(novicemmc),tags='novice',anchor=NW)

        #slika
        self.slikammc=PhotoImage(file='rtv.png')
        self.platno.create_image(452,5,image=self.slikammc,anchor='nw',tags='novice')
        self.nalozi()

    def nalozi(self):

        slikica1=random.choice(slikee)
        slikica2=random.choice(slikee)
        slikica3=random.choice(slikee)

        
        self.sl1=PhotoImage(file=slikica1)
        self.platno.create_image(452,205,image=self.sl1,anchor='nw',tags='novi')

        
        self.sl2=PhotoImage(file=slikica2)
        self.platno.create_image(639,205,image=self.sl2,anchor='nw',tags='novi')


        
        self.sl3=PhotoImage(file=slikica3)
        self.platno.create_image(826,205,image=self.sl3,anchor='nw',tags='novi')


    def oprogramu(self):

        self.platno.delete('novice')
        self.platno.create_rectangle(200,500,800,800,fill='white',tags='novice')
        self.platno.create_text(205,505,text='Program je namenjen prikazu aktualnih novic iz nekaterih najbolj obiskanih\n slovenskih spletnih mest.'
                                'Ob prikazu aktualnih novic vas gumb poveži, \n poveže direktno na spletno mesto novice.',anchor='nw',tags='novice')
        


def printajnogomania(seznam):
    niz=''
    for element in seznam:
        niz+=str(element)+'\n'+'LINK'+'\n'
    return niz

def printajmmc(seznam):
    niz=''
    for element in seznam:
        niz+=str(element)+'\n'+'\n'
    return niz

def printajslovenske(seznam):
    niz=''
    for element in seznam:
        prva,druga=element
        novica=re.search(r'"(.*)"',druga)
        niz+=str(novica.group(1))+'\n'+'\n'+'http://www.slovenskenovice.si/'+str(prva)+'\n'+'\n'
    return niz

def printaj24(seznam):
    niz=''
    for element in seznam:
        niz+=str(element)+'\n'

    return niz

def printajreg(seznam):
    niz=''
    for element in seznam:
        link,novica=element
        niz+=str(novica)+'\n                                                          '+'www.regionalobala.si/novice/{0}'.format(link)+'\n'+'\n'
    return niz





root = Tk()
root.title('Aktualne novice')
program = Novice(root)
root.mainloop()
